let fs = require('fs')


function createDir(dirName, numberOfFiles) {
    if (dirName && numberOfFiles) {
        fs.mkdir(dirName, function (err, _) {
            if (err) {
                console.error(err)
            } else {
                console.log('Directory created successfully')
            }
        })
        let file = 1
        for (let n = 0; n < numberOfFiles; n++) {

            fs.appendFile('./' + dirName + '/' + file + '.json', 'File Created', (err) => {
                if (err) {
                    console.log(err)
                }
            })
            file++
        }

        fs.rm(dirName, {recursive: true}, function (err, _) {
            if (err) {
                console.error(err)
                return
            }
            console.log('Directory removed')
        })
    }
}

module.exports = createDir
