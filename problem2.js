const fs = require("fs");


function callbackHell(file, file1, file2, file3) {
    fs.readFile('./' + file, 'utf8', function (error, data) {
        if (error) {
            console.error(error)
        } else {
            fs.appendFile(file1, data.toUpperCase(), function (error1, data1) {
                if (error1) {
                    console.error(error1)
                } else {
                    console.log('file1 created and data added')
                    fs.appendFile('filesnames.txt', file1, function (error2, _) {
                        if (error2) {
                            console.error(error2)
                        }
                    })
                    fs.readFile('./' + file1, 'utf8', function (error3, data3) {
                        if (error3) {
                            console.error(error3)
                        } else {
                            let dataToWrite = (data3.toLowerCase()).replaceAll('.', '\n')
                            fs.appendFile('./' + file2, dataToWrite, function (error4, data4) {
                                if (error4) {
                                    console.error(error4)
                                } else {
                                    console.log('file2 created and data added')
                                    fs.appendFile('filesnames.txt', '\n' + file2, function (error5, _) {
                                        if (error5) {
                                            console.error(error5)
                                        }
                                    })
                                    fs.readFile('./' + file2, 'utf8', function (error6, data6) {
                                        if (error6) {
                                            console.error(error6)
                                        } else {
                                            let sortedData = (data6.split('\n').sort()).join('\n')
                                            fs.appendFile('./' + file3, sortedData, function (error7, _) {
                                                if (error7) {
                                                    console.error(error7)
                                                } else {
                                                    console.log('file3 created and data added')
                                                    fs.appendFile('filesnames.txt', '\n' + file3, function (error8, _) {
                                                        if (error8) {
                                                            console.error(error8)
                                                        } else {
                                                            fs.readFile('./filesnames.txt', "utf-8", function (error9, data9) {
                                                                if (error9) {
                                                                    console.error(error9)
                                                                } else {
                                                                    let filesToDelete = data9.split('\n')
                                                                    fs.unlink('./'+filesToDelete[0],(err)=>{if(err){console.error(err)}})
                                                                    fs.unlink('./'+filesToDelete[1],(err)=>{if(err){console.error(err)}})
                                                                    fs.unlink('./'+filesToDelete[2],(err)=>{if(err){console.error(err)}})
                                                                    fs.unlink('./filesnames.txt',(err)=>{if(err){console.error(err)}})
                                                                    console.log('files deleted')
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })

                        }
                    })
                }
            })
        }
    })
}

module.exports = callbackHell